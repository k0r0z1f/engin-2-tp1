using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class FishermanBehaviour : MonoBehaviour
{
    private int nodeCmp = 0;
    
    private BehaviourTree tree;
    [SerializeField] private GameObject[] activities;
    private NavMeshAgent agent;
    private Animator animator;

    private float waitingTime;

    [SerializeField] private float minWaitTime = 3f;
    [SerializeField] private float maxWaitTime = 10f;

    private int currentDestination = 0;

    private AnimType currentAnimType;

    public enum AnimType
    {
        Idle,
        Walking,
        Fishing,
        Gathering
    }

    public enum ActionState
    {
        IDLE,
        WORKING
    };

    private ActionState state = ActionState.IDLE;

    private Node.Status treeStatus = Node.Status.RUNNING;

    private bool treePaused = false;
    
    [Range(0, 10)] public int fish = 0;
    private float fishingEnd = 0;
    private bool fishing = false;
    
    private float gatheringEnd = 0;
    private bool gathering = false;

    private bool startedWhistling = false;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        ResetTree();
    }

    private void ResetTree()
    {
        state = ActionState.IDLE;

        treeStatus = Node.Status.RUNNING;

        tree = new();
        
        Leaf waitSomeTime = new Leaf("Wait Some Time", WaitSomeTime);
        Leaf endTree = new Leaf("End Tree", EndTree);
        
        Sequence routine = new Sequence("Routine");
        Sequence eatSomething = new Sequence("Eat Something");
        Sequence goFish = new Sequence("Go Fish");
        Sequence goGather = new Sequence("Go Gather");
        Sequence goForAWalk = new Sequence("Go For A Walk");
        Leaf doFish = new Leaf("Fish", DoFish);
        Leaf doGather = new Leaf("Gather", DoGather);
        Leaf hasGotFish = new Leaf("Has Got Fish", HasFish);
        Leaf goToDocks = new Leaf("Go To The Docks", GoToDocks);
        Leaf goToDocks2 = new Leaf("Go To The Docks 2", GoToDocks2);
        Leaf goToDocks3 = new Leaf("Go To The Docks 3", GoToDocks3);
        Leaf goToForest = new Leaf("Go To Forest", GoToForest);
        Leaf goToForest2 = new Leaf("Go To Forest 2", GoToForest2);
        Leaf goFar = new Leaf("Go Far", GoFar);

        goForAWalk.AddChild(goFar);
        goForAWalk.AddChild(goToForest2);
        goForAWalk.AddChild(goToForest);
        
        goFish.AddChild(goToDocks);
        goFish.AddChild(doFish);
        
        goGather.AddChild(goToForest);

        eatSomething.AddChild(goFish);
        eatSomething.AddChild(goGather);

        routine.AddChild(eatSomething);
        routine.AddChild(goForAWalk);
        //routine.AddChild(endTree);
        
        tree.AddChild(routine);

        tree.PrintTree();
        Debug.Log(Time.time + " New Tree");
    }

    public Node.Status EndTree()
    {
        treeStatus = Node.Status.SUCCESS;
        return Node.Status.SUCCESS;
    }
    
    private void ChangeAnimTo(AnimType to, bool changeCurrent = true)
    {
        if(to != AnimType.Walking)
            animator.SetBool("Walking", false);
        if(to != AnimType.Gathering)
            animator.SetBool("Gathering", false);
        if(to != AnimType.Fishing)
            animator.SetBool("Fishing", false);
        if(to != AnimType.Idle)
            animator.SetBool(to.ToString(), true);
        if(changeCurrent)
            currentAnimType = to;
    }
    
    public Node.Status HasFish()
    {
        if (fish >= 1)
            return Node.Status.FAILURE;
        return Node.Status.SUCCESS;
    }
    
    public Node.Status GoToDocks()
    {
        //currentDestination = 0;
        nodeCmp++;
        Debug.Log("Go To Docks " + nodeCmp);
        return GoToLocation(activities[0].transform.position);
    }
    
    public Node.Status GoToForest()
    {
        //currentDestination = 1;
        nodeCmp++;
        Debug.Log("GoToForest " + nodeCmp);
        return GoToLocation(activities[1].transform.position);
    }

    public Node.Status GoFar()
    {
        //currentDestination = 2;
        nodeCmp++;
        Debug.Log("GoFar " + nodeCmp);
        return GoToLocation(activities[2].transform.position); 
    } 
    
    public Node.Status GoToForest2()
    {
        //currentDestination = 1;
        nodeCmp++;
        Debug.Log("GoToForest2 " + nodeCmp);
        return GoToLocation(activities[3].transform.position);
    }
    
    public Node.Status GoToDocks2()
    {
        //currentDestination = 0;
        nodeCmp++;
        Debug.Log("Go To Docks 2 " + nodeCmp);
        return GoToLocation(activities[4].transform.position);
    }

    public Node.Status GoToDocks3()
    {
        //currentDestination = 0;
        nodeCmp++;
        Debug.Log("GoToDocks3 " + nodeCmp);
        return GoToLocation(activities[5].transform.position);
    }

    public Node.Status DoFish()
    {
        /*if(!fishing)
        {
            //Debug.Log("fishing");
            fishing = true;
            fishingEnd = Time.time + Random.Range(1f, 3f);
            ChangeAnimTo(AnimType.Fishing);
            GameObject.Find("Fisher/FishingRod").GetComponent<MeshRenderer>().enabled = true;
        }
        else if(Time.time >= fishingEnd)
        {
            //Debug.Log("finished");
            fishing = false;
            GameObject.Find("Fisher/FishingRod").GetComponent<MeshRenderer>().enabled = false;
            ChangeAnimTo(AnimType.Idle);
            if (Random.Range(0, 2) == 0)
                return Node.Status.FAILURE;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;*/
        return Node.Status.FAILURE;
    }
    
    public Node.Status DoGather()
    {
        /*if(!gathering)
        {
            gathering = true;
            gatheringEnd = Time.time + Random.Range(10f, 30f);
            ChangeAnimTo(AnimType.Gathering);
        }
        else if(Time.time >= gatheringEnd)
        {
            gathering = false;
            ChangeAnimTo(AnimType.Idle);
            if (Random.Range(0, 2) == 0)
                return Node.Status.FAILURE;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;*/
        return Node.Status.SUCCESS;
    }

    Node.Status WaitSomeTime()
    {
        if (Time.time >= waitingTime)
            return Node.Status.SUCCESS;
        return Node.Status.RUNNING;
    }
    
    Node.Status GoToLocation(Vector3 destination)
    {
        float distanceToTarget = Vector3.Distance(destination, transform.position);
        if (state == ActionState.IDLE)
        {
            agent.SetDestination(destination);
            state = ActionState.WORKING;
            animator.SetBool("Walking", true);
            currentAnimType = AnimType.Walking;
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
        {
            state = ActionState.IDLE;
            return Node.Status.FAILURE;
        }
        else if (distanceToTarget < 2)
        {
            waitingTime = Time.time + Random.Range(minWaitTime, maxWaitTime);
            state = ActionState.IDLE;
            animator.SetBool("Walking", false);
            currentAnimType = AnimType.Idle;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            treePaused = true;
            GetComponent<OldManInteractions>().enabled = true;
            agent.isStopped = true;
            Debug.Log("enter anim: " + currentAnimType);
            ChangeAnimTo(AnimType.Idle, false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetComponent<OldManInteractions>().enabled = false;
            treePaused = false;
            agent.isStopped = false;
            Debug.Log("anim "+ currentAnimType);
            ChangeAnimTo(currentAnimType, false);
        }
    }

    private void Update()
    {
        if (!treePaused)
        {
            if (treeStatus == Node.Status.SUCCESS)
                ResetTree();
            tree.Process();
        }
    }
}
