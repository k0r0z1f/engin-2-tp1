using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class FishermanBehaviourOLD : MonoBehaviour
{
    private BehaviourTree tree;
    public GameObject docks;
    public GameObject forest;
    public GameObject far;
    private NavMeshAgent agent;
    private Animator animator;

    private float waitingTime;

    public enum AnimType
    {
        Idle,
        Walking,
        Fishing,
        Gathering
    }

    public enum ActionState
    {
        IDLE,
        WORKING
    };

    private ActionState state = ActionState.IDLE;

    private Node.Status treeStatus = Node.Status.RUNNING;

    [Range(0, 10)] public int fish = 0;
    private float fishingEnd = 0;
    private bool fishing = false;
    
    private float gatheringEnd = 0;
    private bool gathering = false;

    private bool startedWhistling = false;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        ResetTree();
    }

    private void ResetTree()
    {
        state = ActionState.IDLE;

        treeStatus = Node.Status.RUNNING;

        tree = new();
        Sequence routine = new Sequence("Routine");
        Selector eatSomething = new Selector("Eat Something");
        Sequence goFish = new Sequence("Go Fish");
        Sequence goGather = new Sequence("Go Gather");
        Sequence goForAWalk = new Sequence("Go For A Walk");
        Leaf doFish = new Leaf("Fish", DoFish);
        Leaf doGather = new Leaf("Gather", DoGather);
        Leaf hasGotFish = new Leaf("Has Got Fish", HasFish);
        Leaf goToDocks = new Leaf("Go To The Docks", GoToDocks);
        Leaf goToForest = new Leaf("Go To Forest", GoToForest);
        Leaf goFar = new Leaf("Go Far", GoFar);
        Leaf waitSomeTime = new Leaf("Wait Some Time", WaitSomeTime);

        goFish.AddChild(goToDocks);
        goFish.AddChild(doFish);
        
        goGather.AddChild(goToForest);
        goGather.AddChild(doGather);
        
        eatSomething.AddChild(goFish);
        eatSomething.AddChild(goGather);
        
        goForAWalk.AddChild(goToDocks);
        goForAWalk.AddChild(waitSomeTime);
        goForAWalk.AddChild(goToForest);
        goForAWalk.AddChild(waitSomeTime);
        goForAWalk.AddChild(goFar);
        goForAWalk.AddChild(waitSomeTime);
        
        routine.AddChild(eatSomething);
        routine.AddChild(goForAWalk);
        
        tree.AddChild(routine);

        tree.PrintTree();
    }

    private void ChangeAnimTo(AnimType to)
    {
        animator.SetBool("Walking", false);
        animator.SetBool("Gathering", false);
        animator.SetBool("Fishing", false);
        if(to != AnimType.Idle)
            animator.SetBool(to.ToString(), true);
    }
    
    public Node.Status HasFish()
    {
        if (fish >= 1)
            return Node.Status.FAILURE;
        return Node.Status.SUCCESS;
    }

    public Node.Status GoToDocks()
    {
        return GoToLocation(docks.transform.position);
    }
    
    public Node.Status GoToForest()
    {
        return GoToLocation(forest.transform.position);
    }
    
    public Node.Status GoFar()
    {
        return GoToLocation(far.transform.position);
    }

    public Node.Status DoFish()
    {
        if(!fishing)
        {
            //Debug.Log("fishing");
            fishing = true;
            fishingEnd = Time.time + Random.Range(1f, 3f);
            ChangeAnimTo(AnimType.Fishing);
            GameObject.Find("Fisher/FishingRod").GetComponent<MeshRenderer>().enabled = true;
        }
        else if(Time.time >= fishingEnd)
        {
            //Debug.Log("finished");
            fishing = false;
            GameObject.Find("Fisher/FishingRod").GetComponent<MeshRenderer>().enabled = false;
            ChangeAnimTo(AnimType.Idle);
            if (Random.Range(0, 2) == 0)
                return Node.Status.FAILURE;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }
    
    public Node.Status DoGather()
    {
        if(!gathering)
        {
            gathering = true;
            gatheringEnd = Time.time + Random.Range(10f, 30f);
            ChangeAnimTo(AnimType.Gathering);
        }
        else if(Time.time >= gatheringEnd)
        {
            gathering = false;
            ChangeAnimTo(AnimType.Idle);
            if (Random.Range(0, 2) == 0)
                return Node.Status.FAILURE;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }
    
    Node.Status WaitSomeTime()
    {
        if(!startedWhistling)
        {
            GetComponent<AudioSource>().Play();
            startedWhistling = true;
        }
            
        if (Time.time >= waitingTime)
            return Node.Status.SUCCESS;
        return Node.Status.RUNNING;
    }

    Node.Status GoToLocation(Vector3 destination)
    {
        float distanceToTarget = Vector3.Distance(destination, transform.position);
        if (state == ActionState.IDLE)
        {
            agent.SetDestination(destination);
            state = ActionState.WORKING;
            animator.SetBool("Walking", true);
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
        {
            state = ActionState.IDLE;
            return Node.Status.FAILURE;
        }
        else if (distanceToTarget < 2)
        {
            startedWhistling = false;

            waitingTime = Time.time + Random.Range(4f, 15f);
            state = ActionState.IDLE;
            animator.SetBool("Walking", false);
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }

    private void Update()
    {
        if (treeStatus == Node.Status.SUCCESS)
            ResetTree();
        tree.Process();
    }
}
