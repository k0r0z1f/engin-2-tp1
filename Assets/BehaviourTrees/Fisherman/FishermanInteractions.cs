using System;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class FishermanInteractions : MonoBehaviour
{
    private int nodeCmp = 0;
    
    private BehaviourTree tree;
    private NavMeshAgent agent;
    private Animator animator;
    [SerializeField] private GameObject canvas;

    private float waitingTime;

    [SerializeField] private float minWaitTime = 3f;
    [SerializeField] private float maxWaitTime = 10f;

    private int answer = -1;

    public enum AnimType
    {
        Idle,
        Sitting
    }

    public enum ActionState
    {
        IDLE,
        WORKING
    };

    private ActionState state = ActionState.IDLE;

    private Node.Status treeStatus = Node.Status.RUNNING;

    private void Start()
    {
    }

    private void ResetTree()
    {
        state = ActionState.IDLE;

        treeStatus = Node.Status.RUNNING;

        tree = new();
        
        Leaf waitSomeTime = new Leaf("Wait Some Time", WaitSomeTime);

        Sequence routine = new Sequence("Routine");

        Selector greetings = new Selector("Greetings");
        Leaf happyGreetings = new Leaf("Happy Greetings", HappyGreetings);
        Leaf angryGreetings = new Leaf("Angry Greetings", AngryGreetings);
        Leaf askQuestion = new Leaf("Ask Question", AskQuestion);
        Leaf end = new Leaf("End", EndTree);
        
        Selector processAnswer = new Selector("Process Answer");
        Leaf truthAnswer = new Leaf("Truth Answer", TruthAnswer);
        Leaf foodAnswer = new Leaf("Food Answer", FoodAnswer);
        
        greetings.AddChild(happyGreetings);
        greetings.AddChild(angryGreetings);
        
        processAnswer.AddChild(truthAnswer);
        processAnswer.AddChild(foodAnswer);

        routine.AddChild(greetings);
        routine.AddChild(waitSomeTime);
        routine.AddChild(askQuestion);
        routine.AddChild(waitSomeTime);
        routine.AddChild(processAnswer);
        routine.AddChild(waitSomeTime);
        //routine.AddChild(end);
        tree.AddChild(routine);
        
        tree.PrintTree();
        Debug.Log("new interactions tree");
    }

    private void ChangeAnimTo(AnimType to)
    {
        animator.SetBool("Walking", false);
        animator.SetBool("Sitting", false);
        if(to != AnimType.Idle)
            animator.SetBool(to.ToString(), true);
    }
    
    public Node.Status EndTree()
    {
        treeStatus = Node.Status.SUCCESS;
        return Node.Status.SUCCESS;
    }
    
    public Node.Status HappyGreetings()
    {
        canvas.transform.Find("Image/ChatText").GetComponent<TextMeshProUGUI>().text = "Bien le bonjour étrange voyageuse!";
        //Debug.Log("canvas " + canvas.transform.Find("Image/ChatText").GetComponent<TextMeshProUGUI>());
        waitingTime = Time.time + Random.Range(minWaitTime, maxWaitTime);
        return Node.Status.SUCCESS;
    }
    
    public Node.Status AngryGreetings()
    {
        waitingTime = Time.time + Random.Range(minWaitTime, maxWaitTime);
        return Node.Status.SUCCESS;
    }
    
    public Node.Status AskQuestion()
    {
        canvas.transform.Find("Image/ChatText").GetComponent<TextMeshProUGUI>().text = "Pourquoi êtes-vous venu me voir dans cette montagne isolée?\n1. Je cherche la vérité absolue.\n2. Je meures de faim svp.";
        //Debug.Log("canvas " + canvas.transform.Find("Image/ChatText").GetComponent<TextMeshProUGUI>());
        //if(Input.GetKeyDown(KeyCode.Alpha1))
        if (answer != -1)
            return Node.Status.SUCCESS;

        return Node.Status.RUNNING;
    }
    
    public Node.Status TruthAnswer()
    {
        Debug.Log("truth " + answer);
        if (answer == 2)
            return Node.Status.FAILURE;
        Debug.Log("truth2 " + answer);
        answer = -1;
        canvas.transform.Find("Image/ChatText").GetComponent<TextMeshProUGUI>().text = "La recherche de la vérité est une noble cause, méditez avec moi.";
        waitingTime = Time.time + Random.Range(minWaitTime, maxWaitTime);
        return Node.Status.SUCCESS;
            
    }
    
    public Node.Status FoodAnswer()
    {
        answer = -1;
        canvas.transform.Find("Image/ChatText").GetComponent<TextMeshProUGUI>().text = "La nourriture est toujours un réconfort en ces contrées.";
        waitingTime = Time.time + Random.Range(minWaitTime, maxWaitTime);
        
        return Node.Status.SUCCESS;
        //return Node.Status.FAILURE;
    }

    Node.Status WaitSomeTime()
    {
        if (Time.time >= waitingTime)
            return Node.Status.SUCCESS;
        return Node.Status.RUNNING;
    }
    
    
    Node.Status GoToLocation(Vector3 destination)
    {
        float distanceToTarget = Vector3.Distance(destination, transform.position);
        if (state == ActionState.IDLE)
        {
            agent.SetDestination(destination);
            state = ActionState.WORKING;
            animator.SetBool("Walking", true);
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
        {
            state = ActionState.IDLE;
            return Node.Status.FAILURE;
        }
        else if (distanceToTarget < 2)
        {
            waitingTime = Time.time + Random.Range(minWaitTime, maxWaitTime);
            state = ActionState.IDLE;
            animator.SetBool("Walking", false);
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }

    private void OnEnable()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        
        canvas.GetComponent<Canvas>().enabled = true;
        
        ResetTree();
    }

    private void OnDisable()
    {
        canvas.GetComponent<Canvas>().enabled = false;
    }

    private void Update()
    {
        if(Keyboard.current.digit1Key.wasPressedThisFrame)
        {
            Debug.Log("DETECTED 1");
            answer = 1;
        }
        else if (Keyboard.current.digit2Key.wasPressedThisFrame)
        {
            Debug.Log("DETECTED 1");
            answer = 2;
        }

        if (treeStatus != Node.Status.RUNNING)
            enabled = false;
        tree.Process();
    }

    private void LateUpdate()
    {
        transform.LookAt(GameObject.Find("Betty").transform);
    }
}
