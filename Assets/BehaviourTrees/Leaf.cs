using UnityEngine;

public class Leaf : Node
{
    //private int nodeCmp = 0;
    public delegate Status Tick();

    public Tick ProcessMethod;

    public Leaf() { }

    public Leaf(string n, Tick pm)
    {
        name = n;
        ProcessMethod = pm;
    }
    public override Status Process()
    {
        //nodeCmp++;
        //Debug.Log("Node# " + nodeCmp + ", Leaf: " + name);
        if(ProcessMethod != null)
            return ProcessMethod();
        return Status.FAILURE;
    }
}
