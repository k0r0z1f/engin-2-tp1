using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class OldManBehaviour : MonoBehaviour
{
    private int nodeCmp = 0;
    
    private BehaviourTree tree;
    public GameObject[] wanders;
    public GameObject[] meditations;
    private NavMeshAgent agent;
    private Animator animator;

    private float waitingTime;

    [SerializeField] private float minWaitTime = 3f;
    [SerializeField] private float maxWaitTime = 10f;
    [SerializeField] private float minMeditateTime = 10f;
    [SerializeField] private float maxMeditateTime = 30f;

    private bool meditating;
    private float meditatingEnd;

    private int currentDestination = 0;

    private AnimType currentAnimType;

    public enum AnimType
    {
        Idle,
        Walking,
        Sitting
    }

    public enum ActionState
    {
        IDLE,
        WORKING
    };

    private ActionState state = ActionState.IDLE;

    private Node.Status treeStatus = Node.Status.RUNNING;

    private bool treePaused = false;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        ResetTree();
    }

    private void ResetTree()
    {
        state = ActionState.IDLE;

        treeStatus = Node.Status.RUNNING;

        tree = new();
        
        Leaf waitSomeTime = new Leaf("Wait Some Time", WaitSomeTime);

        Sequence routine = new Sequence("Routine");
        
        Sequence goWander = new Sequence("Go Wander");
        Leaf chooseWander = new Leaf("Choose Wander Site", ChooseWander);
        Leaf goToWanderSite = new Leaf("Go To Wander Site", GoToWanderSite);
        
        Sequence goMeditate = new Sequence("Go Meditate");
        Leaf chooseMeditate = new Leaf("Choose Meditate Site", ChooseMeditate);
        Leaf goToMeditateSite = new Leaf("Go To Meditate Site", GoToMeditateSite);
        Leaf meditate = new Leaf("Meditate", Meditate);
        
        goWander.AddChild(chooseWander);
        goWander.AddChild(goToWanderSite);
        goWander.AddChild(waitSomeTime);
        
        goMeditate.AddChild(chooseMeditate);
        goMeditate.AddChild(goToMeditateSite);
        goMeditate.AddChild(meditate);
        
        routine.AddChild(goMeditate);
        routine.AddChild(goWander);
        tree.AddChild(routine);
        
        tree.PrintTree();
        //Debug.Log(Time.time + " New Tree");
    }

    private void ChangeAnimTo(AnimType to, bool changeCurrent = true)
    {
        if(to != AnimType.Walking)
            animator.SetBool("Walking", false);
        if(to != AnimType.Sitting)
            animator.SetBool("Sitting", false);
        if(to != AnimType.Idle)
            animator.SetBool(to.ToString(), true);
        if(changeCurrent)
            currentAnimType = to;
    }
    
    public Node.Status ChooseWander()
    {
        currentDestination = Random.Range(0, wanders.Length);
        return Node.Status.SUCCESS;
    }
    
    public Node.Status GoToWanderSite()
    {
        return GoToLocation(wanders[currentDestination].transform.position);
    }
    
    public Node.Status ChooseMeditate()
    {
        currentDestination = Random.Range(0, meditations.Length);
        return Node.Status.SUCCESS;
    }
    
    public Node.Status GoToMeditateSite()
    {
        return GoToLocation(meditations[currentDestination].transform.position);
    }
    
    public Node.Status Meditate()
    {
        if(!meditating)
        {
            meditating = true;
            meditatingEnd = Time.time + Random.Range(minMeditateTime, maxMeditateTime);
            ChangeAnimTo(AnimType.Sitting);
        }
        else if(Time.time >= meditatingEnd)
        {
            meditating = false;
            ChangeAnimTo(AnimType.Idle);
            if (Random.Range(0, 2) == 0)
                return Node.Status.FAILURE;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }

    Node.Status WaitSomeTime()
    {
        if (Time.time >= waitingTime)
            return Node.Status.SUCCESS;
        return Node.Status.RUNNING;
    }
    
    
    Node.Status GoToLocation(Vector3 destination)
    {
        float distanceToTarget = Vector3.Distance(destination, transform.position);
        if (state == ActionState.IDLE)
        {
            agent.SetDestination(destination);
            state = ActionState.WORKING;
            animator.SetBool("Walking", true);
            currentAnimType = AnimType.Walking;
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
        {
            state = ActionState.IDLE;
            return Node.Status.FAILURE;
        }
        else if (distanceToTarget < 2)
        {
            waitingTime = Time.time + Random.Range(minWaitTime, maxWaitTime);
            state = ActionState.IDLE;
            animator.SetBool("Walking", false);
            currentAnimType = AnimType.Idle;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            treePaused = true;
            GetComponent<OldManInteractions>().enabled = true;
            agent.isStopped = true;
            //Debug.Log("enter anim: " + currentAnimType);
            if(currentAnimType != AnimType.Sitting)
                ChangeAnimTo(AnimType.Idle, false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetComponent<OldManInteractions>().enabled = false;
            treePaused = false;
            agent.isStopped = false;
            //Debug.Log("anim "+ currentAnimType);
            ChangeAnimTo(currentAnimType, false);
        }
    }

    private void Update()
    {
        if (!treePaused)
        {
            if (treeStatus == Node.Status.SUCCESS)
                ResetTree();
            tree.Process();
        }
    }
}
