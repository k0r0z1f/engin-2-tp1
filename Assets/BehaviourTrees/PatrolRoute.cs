using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PatrolRoute : MonoBehaviour
{
    [SerializeField] private List<GameObject> positions = new List<GameObject>();
    [SerializeField] private List<string> positionName = new List<string>();
    private GameObject positionsContainer;
    private int patrolBehaviour;
    private bool infiniteLoopBehaviour;
    private int infiniteLoopQuantity;
    
    private void Reset()
    {
        if (positionsContainer == null)
        {
            positionsContainer = new GameObject("Positioncontainer of: " + gameObject.name);
        }
    }
    private void AddPosition()
    {
        if(positionsContainer == null) 
            Reset();
        positionName.Add("Set name of Position " + positions.Count);
        positions.Add(new GameObject("Position " + positions.Count));
        positions[positions.Count - 1].transform.SetParent(positionsContainer.transform);
        positions[positions.Count - 1].AddComponent<PickableGizmo>();
        var icon = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Textures/cible.png");
        EditorGUIUtility.SetIconForObject(positions[positions.Count - 1], icon);
    }

    private void RemovePosition()
    {
        if (positions.Any())
        {
            var go = positions[positions.Count - 1];
            if (go != null)
            {
                DestroyImmediate(go);
            }
            positions.RemoveAt(positions.Count - 1);
        }
        
        if (positionName.Any())
        {
            positionName.RemoveAt(positionName.Count - 1);
        }
    }

    private void SetBehaviour(int index)
    {
        patrolBehaviour = index;
    }
    
    private void SetInfiniteLoopQty(string qty)
    {
        if (int.TryParse(qty, out int n))
        {
            infiniteLoopQuantity = Int32.Parse(qty);
        }
    }

    private void ToggleInfiniteLoop(bool infiniteLoop)
    {
        infiniteLoopBehaviour = infiniteLoop;
    }

    public List<GameObject> GetPositions()
    {
        return positions;
    }
    
    public int GetPatrolBehaviour()
    {
        return patrolBehaviour;
    }
    
    [CustomEditor(typeof(PatrolRoute))]
    public class PatrolRouteEditor : Editor
    {
        private SerializedProperty targetPosition;
        private SerializedProperty targetPositionName;

        private void OnEnable()
        {
            targetPosition = serializedObject.FindProperty("positions");
            targetPositionName = serializedObject.FindProperty("positionName");
        }

        public string[] options = new string[] {"Random", "Loop", "PingPong"};
        private int behaviourIndex;
        private bool infiniteLoop = false;
        private string loopQty = "1";
        public override void OnInspectorGUI()
        {
            PatrolRoute ptrRoute = (PatrolRoute)target;
            behaviourIndex = ptrRoute.patrolBehaviour;
            infiniteLoop = ptrRoute.infiniteLoopBehaviour;
            loopQty = ptrRoute.infiniteLoopQuantity.ToString();
            GUILayout.Label("Patrol Behaviour");
            behaviourIndex = EditorGUILayout.Popup(behaviourIndex, options);
            ptrRoute.SetBehaviour(behaviourIndex);
            if (behaviourIndex == 1)
            {
                infiniteLoop = EditorGUILayout.Toggle("Infinite Loop", infiniteLoop);
                ptrRoute.ToggleInfiniteLoop(infiniteLoop);
                if(!infiniteLoop)
                {
                    GUILayout.Label("Nombre de loop");
                    loopQty = GUILayout.TextField(loopQty, GUILayout.MinWidth(30));
                    if(loopQty != ptrRoute.infiniteLoopQuantity.ToString())
                        ptrRoute.SetInfiniteLoopQty(loopQty);
                }
            }
            GUILayout.Label("Add Positions for the NPC or Enemy to patrol");
            if (GUILayout.Button("Add a new Position"))
            {
                ptrRoute.AddPosition();
            }
            
            foreach (var p in ptrRoute.positions)
            {
                int index = ptrRoute.positions.FindIndex(x => x == p);
                EditorGUILayout.BeginHorizontal();
                EditorGUI.BeginChangeCheck();
                ptrRoute.positionName[index] = GUILayout.TextField((ptrRoute.positionName[index]), GUILayout.MinWidth(130));
                if (EditorGUI.EndChangeCheck())
                {
                    ptrRoute.positions[index].name = ptrRoute.positionName[index];
                }
                GUILayout.Space(10);
                
                EditorGUI.BeginChangeCheck();
                UnityEngine.Vector3 pos = EditorGUILayout.Vector3Field("", ptrRoute.positions[index].transform.position, GUILayout.MinWidth(Screen.width * 0.275f)); //à retravailler 
                if (EditorGUI.EndChangeCheck())
                {
                    ptrRoute.positions[index].transform.position = pos;
                }
                EditorGUILayout.EndHorizontal();
                
                Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(1));
                EditorGUI.DrawRect(r, Color.gray);
                
            }

            if (GUILayout.Button("Remove Last Position"))
            {
                    ptrRoute.RemovePosition();
            }
            
            serializedObject.ApplyModifiedProperties();
        }
    }

}
