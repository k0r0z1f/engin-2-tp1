using UnityEngine;

public class Sequence : Node
{
    //private int nodeCmp = 0;
    public Sequence(string n)
    {
        name = n;
    }

    public override Status Process()
    {
        //nodeCmp++;
        //Debug.Log("Node# " + nodeCmp + ", Sequence: " + name);
        Status childstatus = children[currentChild].Process();
        if (childstatus == Status.RUNNING)
            return Status.RUNNING;
        if (childstatus == Status.FAILURE) 
            return childstatus;

        currentChild++;
        if (currentChild >= children.Count)
        {
            currentChild = 0;
            return Status.SUCCESS;
        }
        
        return Status.RUNNING;
    }
}
