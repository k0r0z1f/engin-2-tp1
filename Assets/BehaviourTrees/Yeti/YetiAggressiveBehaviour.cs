using System;
using System.Numerics;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class YetiAggressiveBehaviour : MonoBehaviour
{
    private BehaviourTree tree;
    private NavMeshAgent agent;
    private Animator animator;

    private float waitingTime;
    private bool startedGrowling = false;
    private bool inAction = false;
    private int currentDestination = 0;
    private float intimidateEnd;
    private float actionEnd;

    private enum AnimType
    {
        Idle,
        Running,
        Attacking,
        Growling
    }

    private enum ActionState
    {
        IDLE,
        WORKING
    };

    private ActionState state = ActionState.IDLE;

    private Node.Status treeStatus = Node.Status.RUNNING;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        ResetTree();
    }

    private void ResetTree()
    {
        state = ActionState.IDLE;

        treeStatus = Node.Status.RUNNING;

        tree = new();
        
        Leaf waitSomeTime = new Leaf("Wait Some Time", WaitSomeTime);
        
        Selector attackRoutine = new Selector("AttackRoutine");
        
        Leaf intimidate = new Leaf("Intimidate", Intimidate);
        Leaf chargePlayer = new Leaf("Charge Player", ChargePlayer);
        Leaf retreat = new Leaf("Retreat", Retreat);
        Leaf attackPlayer = new Leaf("Attack Player", AttackPlayer);

        Sequence attackPattern1 = new Sequence("Attack Pattern 1");
        Sequence attackPattern2 = new Sequence("Attack Pattern 2");
        
        attackPattern1.AddChild(intimidate);
        attackPattern1.AddChild(waitSomeTime);
        
        attackPattern2.AddChild(chargePlayer);
        attackPattern1.AddChild(waitSomeTime);
        attackPattern2.AddChild(attackPlayer);
        attackPattern1.AddChild(waitSomeTime);
        
        attackRoutine.AddChild(attackPattern1);
        attackRoutine.AddChild(attackPattern2);

        tree.AddChild(attackRoutine);
        tree.PrintTree();
    }

    private void ChangeAnimTo(AnimType to)
    {
        animator.SetBool("Running", false);
        animator.SetBool("Growling", false);
        if(to != AnimType.Idle)
            animator.SetBool(to.ToString(), true);
    }
    
    public Node.Status Intimidate()
    {
        gameObject.transform.LookAt(GameObject.Find("Betty").transform);
        if (!startedGrowling)
        {
            intimidateEnd = Time.time + 5f;
            startedGrowling = true;
            ChangeAnimTo(AnimType.Growling);
            GetComponent<AudioSource>().Play();
        }
        else if(Time.time >= intimidateEnd)
        {
            startedGrowling = false;
            ChangeAnimTo(AnimType.Idle);
            if (Random.Range(0, 2) == 0)
                return Node.Status.SUCCESS;
            return Node.Status.FAILURE;
        }

        return Node.Status.RUNNING;
    }
    
    public Node.Status ChargePlayer()
    {
        if (!inAction)
        {
            agent.isStopped = false;
            inAction = true;
        }
        else
        {
            return GoToLocation(GameObject.Find("Betty").transform.position);
        }

        return Node.Status.RUNNING;
    }
    
    public Node.Status Retreat()
    {
        return Node.Status.SUCCESS;
    }
    
    public Node.Status AttackPlayer()
    {
        if (!inAction)
        {
            inAction = true;
            actionEnd = Time.time + Random.Range(3f, 6f);
            ChangeAnimTo(AnimType.Attacking);
        }
        else if(Time.time >= actionEnd)
        {
            inAction = false;
            ChangeAnimTo(AnimType.Idle);
            int hitPointDamage = Random.Range(0, 3);
            if (hitPointDamage != 0)
            {
                GameObject.Find("Betty").GetComponent<PlayerController>().TakeDamage(hitPointDamage);
                return Node.Status.SUCCESS;
            }
            return Node.Status.FAILURE;
        }

        return Node.Status.RUNNING;
    }
    
    Node.Status WaitSomeTime()
    {
        if(!startedGrowling)
        {
            GetComponent<AudioSource>().Play();
            startedGrowling = true;
        }
            
        if (Time.time >= waitingTime)
            return Node.Status.SUCCESS;
        return Node.Status.RUNNING;
    }
    Node.Status GoToLocation(Vector3 destination)
    {
        float distanceToTarget = Vector3.Distance(destination, transform.position);
        if (state == ActionState.IDLE)
        {
            agent.SetDestination(destination);
            state = ActionState.WORKING;
            animator.SetBool("Running", true);
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2f)
        {
            state = ActionState.IDLE;
            return Node.Status.FAILURE;
        }
        else if (distanceToTarget < 2f)
        {
            startedGrowling = false;
            inAction = false;

            waitingTime = Time.time + Random.Range(4f, 15f);
            state = ActionState.IDLE;
            animator.SetBool("Running", false);
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }

    private void OnEnable()
    {
        ResetTree();
    }

    private void Update()
    {
        if (treeStatus == Node.Status.SUCCESS)
            ResetTree();
        tree.Process();
    }
}
