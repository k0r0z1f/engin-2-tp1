using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class YetiBehaviour : MonoBehaviour
{
    private BehaviourTree tree;
    private List<GameObject> patrols;
    private int patrolBehaviour;
    private NavMeshAgent agent;
    private Animator animator;
    private AnimType currentAnimType;

    private int pingPongIndex = -1;
    private bool pingPongInverse = false;
    
    private int loopIndex = -1;
    private int loopQty = -1;

    private float waitingTime;

    private enum AnimType
    {
        Idle,
        Running,
        Attacking,
        Growling
    }

    private enum ActionState
    {
        IDLE,
        WORKING
    };

    private ActionState state = ActionState.IDLE;
    private Node.Status treeStatus = Node.Status.RUNNING;
    private bool pauseTree = false;

    private bool startedGrowling = false;
    
    private int currentDestination = 0;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        ResetTree();
    }

    private void ResetTree()
    {
        state = ActionState.IDLE;

        treeStatus = Node.Status.RUNNING;

        tree = new();

        Sequence routine = new Sequence("Routine");
        Sequence goPatrol = new Sequence("Go Patrol");
        Leaf choosePatrolRoute = new Leaf("Choose Patrol Route", ChoosePatrolRoute);
        Leaf goToPatrolLocation = new Leaf("Go To Patrol Location", GoToPatrolLocation);
        Leaf waitSomeTime = new Leaf("Wait Some Time", WaitSomeTime);
        
        goPatrol.AddChild(choosePatrolRoute);
        goPatrol.AddChild(goToPatrolLocation);
        goPatrol.AddChild(waitSomeTime);
        
        routine.AddChild(goPatrol);
        
        tree.AddChild(routine);
        
        tree.PrintTree();
    }

    private void ChangeAnimTo(AnimType to)
    {
        animator.SetBool("Running", false);
        animator.SetBool("Attacking", false);
        animator.SetBool("Growling", false);
        if(to != AnimType.Idle)
            animator.SetBool(to.ToString(), true);
        currentAnimType = to;
    }
    
    public Node.Status ChoosePatrolRoute()
    {
        //patrolBehaviour = GetComponent<PatrolRoute>().GetPatrolBehaviour();
        //patrols = GetComponent<PatrolRoute>().GetPositions();
        if (patrolBehaviour == 0)
        {
            currentDestination = Random.Range(0, patrols.Count);
        }
        else if (patrolBehaviour == 1)
        {
            loopIndex = loopIndex + 1 >= patrols.Count ? 0 : loopIndex + 1;
            currentDestination = loopIndex;
        }
        else if (patrolBehaviour == 2)
        {
            pingPongIndex = pingPongInverse ? pingPongIndex - 1 : pingPongIndex + 1;
            if (pingPongIndex >= patrols.Count)
            {
                pingPongIndex--;
                pingPongInverse = true;
            } else if (pingPongIndex < 0)
            {
                pingPongIndex++;
                pingPongInverse = false;
            }
            currentDestination = pingPongIndex;
        }
        return Node.Status.SUCCESS;
    }
    
    public Node.Status GoToPatrolLocation()
    {
        patrols = GetComponent<PatrolRoute>().GetPositions();
        return GoToLocation(patrols[currentDestination].transform.position);
    }

    Node.Status WaitSomeTime()
    {
        if(!startedGrowling)
        {
            GetComponent<AudioSource>().Play();
            startedGrowling = true;
        }
            
        if (Time.time >= waitingTime)
            return Node.Status.SUCCESS;
        return Node.Status.RUNNING;
    }
    Node.Status GoToLocation(Vector3 destination)
    {
        float distanceToTarget = Vector3.Distance(destination, transform.position);
        if (state == ActionState.IDLE)
        {
            agent.SetDestination(destination);
            state = ActionState.WORKING;
            animator.SetBool("Running", true);
            currentAnimType = AnimType.Running;
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
        {
            state = ActionState.IDLE;
            return Node.Status.FAILURE;
        }
        else if (distanceToTarget < 2)
        {
            startedGrowling = false;
            waitingTime = Time.time + Random.Range(4f, 15f);
            state = ActionState.IDLE;
            animator.SetBool("Running", false);
            currentAnimType = AnimType.Idle;
            return Node.Status.SUCCESS;
        }

        return Node.Status.RUNNING;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            pauseTree = true;
            GetComponent<YetiAggressiveBehaviour>().enabled = true;
            agent.isStopped = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetComponent<YetiAggressiveBehaviour>().enabled = false;
            pauseTree = false;
            agent.isStopped = false;
            ChangeAnimTo(currentAnimType);
        }
    }

    private void Update()
    {
        if(!pauseTree)
        {
            patrolBehaviour = GetComponent<PatrolRoute>().GetPatrolBehaviour();
            patrols = GetComponent<PatrolRoute>().GetPositions();
            if (treeStatus == Node.Status.SUCCESS)
                ResetTree();
            tree.Process();
        }
    }
}
