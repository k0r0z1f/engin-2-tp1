using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateWithMouseAxis : MonoBehaviour, CameraInputs.ICameraRotationActions
{
    private CameraInputs cInputs;
    private Vector2 mousePos;
    [SerializeField] private float rotSpeed = 0.5f;
    private bool isRotating;
    [SerializeField] private Transform playerTransform;

    private void OnEnable()
    {
        cInputs.Enable();
    }

    private void OnDisable()
    {
        cInputs.Disable();
    }

    private void Awake()
    {
        cInputs = new CameraInputs();
        cInputs.CameraRotation.SetCallbacks(this);
    }

    public void OnMousePosition(InputAction.CallbackContext context)
    {
        mousePos = context.ReadValue<Vector2>();
    }

    private void FixedUpdate()
    {
        transform.rotation = Quaternion.AngleAxis(mousePos.x * rotSpeed, Vector3.up);
    }

    private void LateUpdate()
    {
        playerTransform.rotation = Quaternion.AngleAxis(mousePos.x * rotSpeed, Vector3.up);
    }
}
