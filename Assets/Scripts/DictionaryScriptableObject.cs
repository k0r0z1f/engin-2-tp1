﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dictionary Storage", menuName = "Data Objects/Dictionary Storage Object")]
public class DictionaryScriptableObject : ScriptableObject
{
    [SerializeField]
    List<string> keys = new List<string>();
    [SerializeField]
    List<Vector3> values = new List<Vector3>();

    public List<string> Keys { get => keys; set => keys = value; }
    public List<Vector3> Values { get => values; set => values = value; }
}
