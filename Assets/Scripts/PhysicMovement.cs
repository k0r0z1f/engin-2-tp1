using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[System.Serializable]
public class PhysicMovement : MonoBehaviour
{
    private Rigidbody _rigidbody;
    protected ForceMode _forceMode;
    protected int _groundSpeed = 8;
    protected int _maxGroundSpeed = 8;

    protected void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    protected void Move(Vector3 dir)
    {
        _rigidbody.AddForce(dir * _groundSpeed, _forceMode);
    }
}
