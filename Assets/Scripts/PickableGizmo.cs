using UnityEngine;
using UnityEngine.AI;

public class PickableGizmo : MonoBehaviour
{
    void OnDrawGizmos()
    {
        var offset = new Vector3(0, 10, 0);

        NavMeshHit hit;
        if(NavMesh.SamplePosition(transform.position, out hit, 0.1f, NavMesh.AllAreas))
        {
            Gizmos.color = Color.green;
        }
        else
        {
            Gizmos.color = Color.red;
        }
        
        Gizmos.DrawLine(transform.position, transform.position + offset);
        Gizmos.DrawSphere(transform.position + offset, 1);
    }
}