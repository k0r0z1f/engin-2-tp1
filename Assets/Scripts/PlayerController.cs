using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour, PlayerInputs.IPlayerActionsActions
{
    //Inputs
    private PlayerInputs _playerInputs;
    public Dictionary<string, bool> InputTriggers = new();
    
    //States
    private PlayerState _currentState;
    public RunningState _runningState;
    public WalkingState _walkingState;
    public JumpingState _jumpingState;
    public FallingState _fallingState;
    public IdleState _idleState;
    public StrafeLeftState _strafeLeftState;
    public StrafeRightState _strafeRightState;
    public DieState _dieState;
    public HurtState _hurtState;

    //Grounded
    [SerializeField] private LayerMask layerMask;
    Vector3 _bottomCenter => new Vector3(_col.bounds.center.x, _col.bounds.min.y, _col.bounds.center.z) + new Vector3(0, 0.01f, 0);
    RaycastHit hit; 
    public bool isGrounded => Physics.Raycast(_bottomCenter, Vector3.down, out hit, 0.5f, layerMask);
    
    //Components
    public Animator _animator;
    public CapsuleCollider _col;
    public Rigidbody rb;
    
    //Health
    [SerializeField]
    private int hitPoints = 5;
    
    
//Inputs: <-
    enum State : sbyte
    {
        Walking,
        Running,
        StrafeLeft,
        StrafeRight,
        Die,
        Hurt
    };

    [SerializeField] private List<State> _activeStates = new List<State>();
    private ICharacterState walk, run, strafeLeft, strafeRight, grounded, jump, die, hurt;
    
    private PlayerInputs pInputs;

    delegate bool PlayerMoveState(PlayerController pc);

    private PlayerMoveState _playerMoveState;
    [System.NonSerialized] public Vector2 inputMoveDirection;


    void Awake()
    {
        AddStatesComponents();
        pInputs = new PlayerInputs();
        pInputs.PlayerActions.SetCallbacks(this);
        
        //Input Dictionary
        _playerInputs = new PlayerInputs(); //input c# script
        InputTriggers.Add("Walk", false);
        InputTriggers.Add("Jump", false);
        InputTriggers.Add("Run", false);
        InputTriggers.Add("StrafeLeft", false);
        InputTriggers.Add("StrafeRight", false);
        _playerInputs.PlayerActions.Walk.performed += OnInputTrigger;
        _playerInputs.PlayerActions.Walk.canceled += OnInputTrigger;
        _playerInputs.PlayerActions.Run.performed += OnInputTrigger;
        _playerInputs.PlayerActions.Run.canceled += OnInputTrigger;
        _playerInputs.PlayerActions.Jump.performed += OnInputTrigger;
        _playerInputs.PlayerActions.Jump.canceled += OnInputTrigger;
        _playerInputs.PlayerActions.StrafeLeft.performed += OnInputTrigger;
        _playerInputs.PlayerActions.StrafeLeft.canceled += OnInputTrigger;
        _playerInputs.PlayerActions.StrafeRight.performed += OnInputTrigger;
        _playerInputs.PlayerActions.StrafeRight.canceled += OnInputTrigger;
        
        //Components
        _animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        _col = GetComponent<CapsuleCollider>();
        
        //States
        _runningState = new RunningState(this);
        _walkingState = new WalkingState(this);
        _jumpingState = new JumpingState(this);
        _fallingState = new FallingState(this);
        _idleState = new IdleState(this);
        _strafeLeftState = new StrafeLeftState(this);
        _strafeRightState = new StrafeRightState(this);
        _dieState = new DieState(this);
        _hurtState = new HurtState(this);
    }
    
    private void Start()
    {
        ChangeState(_idleState);
    }

    public void  ChangeState(PlayerState state)
    {
        _currentState?.OnExitState();
        _currentState = state;
        _currentState.OnEnterState();
    }
    
    void Update()
    {
        _currentState.OnUpdateState(); 
    }
    
    void OnEnable()
    {
        pInputs.Enable();
        _playerInputs.PlayerActions.Enable();
    }

    void OnDisable()
    {
        pInputs.Disable();
        _playerInputs.PlayerActions.Disable();
    }
    
    //Inputs: ->
    void OnInputTrigger(InputAction.CallbackContext context)
    {
        
        if (context.action.phase == InputActionPhase.Performed)
        {
            InputTriggers[context.action.name] = true;
            return;
        }
        if (context.action.phase == InputActionPhase.Canceled)
        {
            InputTriggers[context.action.name] = false;
        }
    }
    private void AddStatesComponents()
    {
        walk = gameObject.AddComponent<Walk>();
        run = gameObject.AddComponent<Run>();
        grounded = gameObject.AddComponent<Grounded>();
        jump = gameObject.AddComponent<Jump>();
        strafeLeft = gameObject.AddComponent<StrafeLeft>();
        strafeRight = gameObject.AddComponent<StrafeRight>();
        die = gameObject.AddComponent<Die>();
        hurt = gameObject.AddComponent<Hurt>();
    }

    private void AddMoveState(ICharacterState state, State newState)
    {
        _playerMoveState += state.StateHandle;
        if(!_activeStates.Contains(newState)) 
            _activeStates.Add(newState);
    }
    
    private void RemoveMoveState(ICharacterState state, State endedState)
    {
        _playerMoveState -= state.StateHandle;
        if(_activeStates.Contains(endedState)) 
            _activeStates.Remove(endedState);
    }

    private void FixedUpdate()
    {
        if (_playerMoveState != null) 
            _playerMoveState(this);
    }

    public void OnWalk(InputAction.CallbackContext value)
    {
        inputMoveDirection = value.ReadValue<Vector2>();
        if (value.started)
        {
            if (!_activeStates.Contains(State.Running))
            {
                AddMoveState(walk, State.Walking);
            }
        }

        if (value.canceled)
        {
            inputMoveDirection = Vector2.zero;
            if (_activeStates.Contains(State.Walking))
            {
                RemoveMoveState(walk, State.Walking);
            }
        }
    }
    
    public void OnRun(InputAction.CallbackContext value)
    {
            if (value.started)
            {
                AddMoveState(run, State.Running);
                if (_activeStates.Contains(State.Walking))
                {
                    RemoveMoveState(run, State.Walking);
                }
            }

            if (value.canceled)
            {
                RemoveMoveState(run, State.Running);
                if (inputMoveDirection != Vector2.zero)
                {
                    AddMoveState(walk, State.Walking);
                }
            }
    }
    
    public void OnJump(InputAction.CallbackContext value)
    {
        if (value.started && OnIsGrounded())
        {
            jump.StateHandle(this);
        }
    }

    public void OnStrafeLeft(InputAction.CallbackContext value)
    {
        inputMoveDirection = value.ReadValue<Vector2>();
        if (value.started)
        {
            AddMoveState(strafeLeft, State.StrafeLeft);
        }

        if (value.canceled)
        {
            inputMoveDirection = Vector2.zero;
            if (_activeStates.Contains(State.StrafeLeft))
            {
                RemoveMoveState(strafeLeft, State.StrafeLeft);
            }
        }
    }
    
    public void OnStrafeRight(InputAction.CallbackContext value)
    {
        inputMoveDirection = value.ReadValue<Vector2>();
        if (value.started)
        {
            AddMoveState(strafeRight, State.StrafeRight);
        }

        if (value.canceled)
        {
            inputMoveDirection = Vector2.zero;
            if (_activeStates.Contains(State.StrafeRight))
            {
                RemoveMoveState(strafeRight, State.StrafeRight);
            }
        }
    }

    private bool OnIsGrounded()
    {
        return grounded.StateHandle(this);
    }

    public void TakeDamage(int dam)
    {
        hitPoints -= dam;
        
        if(hitPoints!=0)
            ChangeState(_hurtState);
        
        if (hitPoints <= 0)
            ChangeState(_dieState);
    }
}
