using UnityEngine;

public class Die : PhysicMovement, ICharacterState
{
    new void Awake()
    {
        base.Awake();
        _groundSpeed = 8;
        _maxGroundSpeed = 8;
        _forceMode = ForceMode.Force;
    }
    public bool StateHandle(PlayerController pc)
    {
        return true;
    }
}