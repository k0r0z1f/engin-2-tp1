public class FallingState : PlayerState
{
    public FallingState(PlayerController player) : base(player)
    {
    }

    override public void OnEnterState()
    {
        playerC._animator.SetBool("Fall", true);
    }

    override public void OnUpdateState()
    {
        if (playerC.isGrounded)
        {
            playerC.ChangeState(playerC._idleState);
        }
    }

    override public void OnExitState()
    {
        playerC._animator.SetBool("Fall", false);
    }
}