public class IdleState : PlayerState
{
    public IdleState(PlayerController player) : base(player)
    {
    }

    override public void OnEnterState()
    {
    }

    override public void OnUpdateState()
    {
        if (!playerC.isGrounded)
        {
            playerC.ChangeState(playerC._fallingState);
            return;
        }

        if (playerC.InputTriggers["Jump"])
        {
            playerC.ChangeState(playerC._jumpingState);
            return;
        }

        if (playerC.InputTriggers["Walk"])
        {
            playerC.ChangeState(playerC._walkingState);
            return;
        }
        
        if (playerC.InputTriggers["Run"])
        {
            playerC.ChangeState(playerC._runningState);
            return;
        }
        
        if (playerC.InputTriggers["StrafeLeft"])
        {
            playerC.ChangeState(playerC._strafeLeftState);
            return;
        }
        
        if (playerC.InputTriggers["StrafeRight"])
        {
            playerC.ChangeState(playerC._strafeRightState);
        }
    }

    override public void OnExitState()
    {
    }
}