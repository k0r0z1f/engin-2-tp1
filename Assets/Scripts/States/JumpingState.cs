using System.Timers;
using UnityEngine;
using Object = System.Object;

public class JumpingState : PlayerState
{
    private float jumpForce = 5;
    private bool jumpBuffer = false;
    private static Timer bufferTimer;

    public JumpingState(PlayerController player) : base(player)
    {
        bufferTimer = new Timer(100);
        bufferTimer.AutoReset = false;
        bufferTimer.Elapsed += SetJumpBuffer;
    }

    override public void OnEnterState()
    {
        playerC._animator.SetBool("Jump", true);
        playerC.rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        bufferTimer.Enabled = true;
    }

    private void SetJumpBuffer(Object source, ElapsedEventArgs e)
    {
        jumpBuffer = true;
    }

    override public void OnUpdateState()
    {
        if (!jumpBuffer) return;
        if (!playerC.isGrounded)
        {
            playerC.ChangeState(playerC._fallingState);
        }
        else
        {
            playerC.ChangeState(playerC._idleState);
        }
    }

    override public void OnExitState()
    {
        playerC._animator.SetBool("Jump", false);
        jumpBuffer = false;
    }
}