public abstract class PlayerState
{
    protected PlayerController playerC;

    public PlayerState(PlayerController player)
    {
        playerC = player;
    }
    
    public abstract void OnEnterState();
    public abstract void OnUpdateState();
    public abstract void OnExitState();
}