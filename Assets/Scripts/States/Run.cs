using UnityEngine;

public class Run : PhysicMovement, ICharacterState
{
    new void Awake()
    {
        base.Awake();
        _groundSpeed = 15;
        _maxGroundSpeed = 15;
        _forceMode = ForceMode.Force;
    }
    public bool StateHandle(PlayerController pc)
    {
        Vector3 dir = new Vector3(pc.inputMoveDirection.x, 0, pc.inputMoveDirection.y);
        Move(dir);
        return true;
    }
}
