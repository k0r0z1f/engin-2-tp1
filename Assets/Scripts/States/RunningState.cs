public class RunningState : PlayerState
{
    public RunningState(PlayerController player) : base(player)
    {
    }

    override public void OnEnterState()
    {
        playerC._animator.SetBool("Run", true);
    }

    override public void OnUpdateState()
    {
        if (!playerC.isGrounded)
        {
            playerC.ChangeState(playerC._fallingState);
            return;
        }

        if (playerC.InputTriggers["Jump"])
        {
            playerC.ChangeState(playerC._jumpingState);
            return;
        }

        if (!playerC.InputTriggers["Run"] || !playerC.InputTriggers["Walk"])
        {
            playerC.ChangeState(playerC._walkingState);
        }
    }

    override public void OnExitState()
    {
        playerC._animator.SetBool("Run", false);
    }
}