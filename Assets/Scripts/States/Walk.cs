using UnityEngine;

public class Walk : PhysicMovement, ICharacterState
{
    new void Awake()
    {
        base.Awake();
        _groundSpeed = 8;
        _maxGroundSpeed = 8;
        _forceMode = ForceMode.Force;
    }
    public bool StateHandle(PlayerController pc)
    {
        Vector3 dir = pc.transform.forward * (pc.inputMoveDirection.y * 2f);
        dir += pc.transform.right * (pc.inputMoveDirection.x * 2f);
        Move(dir);
        return true;
    }
}
