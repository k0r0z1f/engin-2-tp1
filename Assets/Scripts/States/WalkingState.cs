public class WalkingState : PlayerState
{
    public WalkingState(PlayerController player) : base(player)
    {
    }

    override public void OnEnterState()
    {
        playerC._animator.SetBool("Walk", true);
    }

    override public void OnUpdateState()
    {
        if (!playerC.isGrounded)
        {
            playerC.ChangeState(playerC._fallingState);
            return;
        }

        if (playerC.InputTriggers["Jump"])
        {
            playerC.ChangeState(playerC._jumpingState);
            return;
        }

        if (!playerC.InputTriggers["Walk"])
        {
            playerC.ChangeState(playerC._idleState);
            return;
        }

        if (playerC.InputTriggers["Run"])
        {
            playerC.ChangeState(playerC._runningState);
        }
    }

    override public void OnExitState()
    {
        playerC._animator.SetBool("Walk", false);
    }
}