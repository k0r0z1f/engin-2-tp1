using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class TeleportTools : MonoBehaviour, ISerializationCallbackReceiver
{
    GUIContent buttonText = new GUIContent("Teleport To");
    [SerializeField] private GameObject characterToTeleport;
    [SerializeField] private GameObject newLocationMarker;
    private Dictionary<string, Vector3> locations;
    
    [SerializeField]
    private DictionaryScriptableObject dictionaryData;

    [SerializeField]
    private List<string> keys = new List<string>();
    [SerializeField]
    private List<Vector3> values = new List<Vector3>();

    private Dictionary<string, Vector3> myDictionary = new Dictionary<string, Vector3>();

    public bool modifyValues;

    private void Awake()
    {
        locations = new Dictionary<string, Vector3>();
        locations.Add("Temple", new Vector3(-149.77f, 923.88f,-360.01f));
        locations.Add("Fisherman's hut", new Vector3(-98.7699966f,132.789993f,457.559998f)); 
        myDictionary.Add("Temple", new Vector3(-149.77f, 923.88f,-360.01f));
        myDictionary.Add("Fisherman's hut", new Vector3(-98.7699966f,132.789993f,457.559998f));
        for (int i = 0; i < Mathf.Min(dictionaryData.Keys.Count, dictionaryData.Values.Count); i++)
        {
            myDictionary.Add(dictionaryData.Keys[i], dictionaryData.Values[i]);
        }
    }

    void OnGUI()
    {
        Rect rt = GUILayoutUtility.GetRect(buttonText, EditorStyles.toolbarButton);
        
        if (GUI.Button(rt, buttonText, EditorStyles.toolbarButton))
        {
            Rect rect = new Rect(EditorGUIUtility.GetMainWindowPosition().x + Screen.mainWindowPosition.x, EditorGUIUtility.GetMainWindowPosition().y + Screen.mainWindowPosition.y, 200, 60);
            TeleportUI dropdown = new TeleportUI(new AdvancedDropdownState(), ref myDictionary, ref characterToTeleport);
            dropdown.Show(rect);
        }
        
        Rect rtLoc = GUILayoutUtility.GetRect(new GUIContent("Add location"), EditorStyles.toolbarButton);

        if (GUI.Button(rtLoc, new GUIContent("Add location"), EditorStyles.toolbarButton))
        {
            myDictionary.Add("Temporary Teleport", newLocationMarker.transform.position);
        }
    }
    
    
    public void OnBeforeSerialize()
    {
        if (modifyValues == false)
        {
            keys.Clear();
            values.Clear();
            for (int i = 0; i < Mathf.Min(dictionaryData.Keys.Count, dictionaryData.Values.Count); i++)
            {
                keys.Add(dictionaryData.Keys[i]);
                values.Add(dictionaryData.Values[i]);
            }
        }
    }

    public void OnAfterDeserialize()
    {
        
    }

    public void DeserializeDictionary()
    {
        myDictionary = new Dictionary<string, Vector3>();
        dictionaryData.Keys.Clear();
        dictionaryData.Values.Clear();
        for (int i = 0; i < Mathf.Min(keys.Count, values.Count); i++)
        {
            dictionaryData.Keys.Add(keys[i]);
            dictionaryData.Values.Add(values[i]);
            myDictionary.Add(keys[i], values[i]);
        }
        modifyValues = false;
    }

    public void PrintDictionary()
    {
        foreach (var pair in myDictionary)
        {
            Debug.Log("Key: " + pair.Key + " Value: " + pair.Value);
        }
    }
}