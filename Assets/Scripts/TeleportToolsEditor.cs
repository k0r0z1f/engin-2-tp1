﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TeleportTools))]
public class TeleportToolsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (((TeleportTools)target).modifyValues)
        {
            if (GUILayout.Button("Save changes"))
            {
                ((TeleportTools)target).DeserializeDictionary();
            }

        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        if (GUILayout.Button("Print Dictionary"))
        {
            ((TeleportTools)target).PrintDictionary();
        }
    }
}
