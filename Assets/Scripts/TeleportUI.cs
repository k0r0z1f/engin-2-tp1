using System.Collections.Generic;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

class TeleportUI : AdvancedDropdown
{
    private Dictionary<string, Vector3> locations;
    private GameObject characterToTeleport;

    public TeleportUI(AdvancedDropdownState state, ref Dictionary<string, Vector3> loc, ref GameObject character) : base(state)
    {
        locations = loc;
        characterToTeleport = character;
    }

    protected override AdvancedDropdownItem BuildRoot()
    {
        AdvancedDropdownItem root = new AdvancedDropdownItem("Locations");

        foreach (var pos in locations)
        {
            AdvancedDropdownItem item = new AdvancedDropdownItem(pos.Key.ToString());
            root.AddChild(item);
        }

        return root;
    }
    protected override void ItemSelected(AdvancedDropdownItem item)
    {
        //Debug.Log(item.name);
        characterToTeleport.transform.position = locations[item.name];
    }
}