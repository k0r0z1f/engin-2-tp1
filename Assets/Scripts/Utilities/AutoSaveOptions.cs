using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class AutoSaveOptions : EditorWindow
{

    delegate void SaveOptions();
    private SaveOptions saveOption;

    private int last;
    
    private bool autosave;
    private bool optionTime;
    private bool optionDirty;
    private bool sceneChanged = false;
    private bool optionPlay;
    private bool playModeChanged = false;
    private bool optionsSection;
    private float lastSave = -1f;
    [MenuItem("Alexis/Autosave")]
    public static void ShowWindow()
    {
        GetWindow(typeof(AutoSaveOptions));
    }

    private void OnGUI()
    {
        if(Time.realtimeSinceStartup >= lastSave + 10f || lastSave == -1f)
        {
#if UNITY_EDITOR
            EditorApplication.playModeStateChanged -= LogPlayModeState;
            EditorApplication.playModeStateChanged += LogPlayModeState;
#endif
        }
        autosave = GUILayout.Toggle(autosave, "Autosave Enabled");
        optionsSection =
            EditorGUILayout.BeginFoldoutHeaderGroup(optionsSection, "Select what kind of save you want");
        if (optionsSection)
        {
            if(last == -1)
                EditorGUILayout.HelpBox("Select at least one option", MessageType.Warning, true);
            optionTime = GUILayout.Toggle(optionTime, "Time");
            optionDirty = GUILayout.Toggle(optionDirty, "Dirty");
            optionPlay = GUILayout.Toggle(optionPlay, "Play");

                if (saveOption != null)
                {
                    saveOption();
                }
                else if(last == -1)
                {
                    Debug.LogWarning("You must select at least one option");
                }
        }
        
        if(EditorGUI.EndChangeCheck())
        {
            OnUpdateDelegate();
        }

    }

    void OnUpdateDelegate()
    {
        saveOption = null;
        if (optionTime && last != 0)
        {
            last = 0;
        } else if (optionDirty && last != 1)
        {
            last = 1;
        } else if (optionPlay && last != 2)
        {
            last = 2;
        }
        else if(!optionTime && !optionDirty && !optionPlay)
        {
            last = -1;
        }

        if (last == 0)
        {
            optionDirty = false;
            optionPlay = false;
            if(autosave)
            {
                saveOption += OnOptionTime;
            }
        }
        else if (last == 1)
        {
            optionTime = false;
            optionPlay = false;
            if (autosave)
            {
                saveOption += OnOptionDirty;
            }
        }
        else if (last == 2)
        {
            optionTime = false;
            optionDirty = false;
            if (autosave)
            {
                saveOption += OnOptionPlay;
            }
        }
    }

    void OnFocus()
    {
        if (EditorPrefs.HasKey("optionsSection"))
            optionsSection = EditorPrefs.GetBool("optionsSection");
        if (EditorPrefs.HasKey("optionTime"))
            optionTime = EditorPrefs.GetBool("optionTime");
        if (EditorPrefs.HasKey("optionDirty"))
            optionDirty = EditorPrefs.GetBool("optionDirty");
        if (EditorPrefs.HasKey("optionPlay"))
            optionPlay = EditorPrefs.GetBool("optionPlay");
    }

    private void OnLostFocus()
    {
        EditorPrefs.SetBool("optionsSection", optionsSection);
        EditorPrefs.SetBool("optionTime", optionTime);
        EditorPrefs.SetBool("optionDirty", optionDirty);
        EditorPrefs.SetBool("optionPlay", optionPlay);
    }

    private void OnDestroy()
    {
        EditorPrefs.SetBool("optionsSection", optionsSection);
        EditorPrefs.SetBool("optionTime", optionTime);
        EditorPrefs.SetBool("optionDirty", optionDirty);
        EditorPrefs.SetBool("optionPlay", optionPlay);
    }
    
    void OnOptionTime()
    {
        if(Time.realtimeSinceStartup >= lastSave + 10f || lastSave == -1f)
        {
            lastSave = Time.realtimeSinceStartup;
            EditorApplication.SaveScene();
        }
    }
    
    public void OnOptionDirty()
    {
        if(Time.realtimeSinceStartup >= lastSave + 10f || lastSave == -1f)
        {
#if UNITY_EDITOR
            EditorSceneManager.sceneDirtied += SceneChanged;
#endif
            lastSave = Time.realtimeSinceStartup;
            sceneChanged = false;
        }
    }
    
    void OnOptionPlay()
    {
        if (Time.realtimeSinceStartup >= lastSave + 10f || lastSave == -1f)
        {
            lastSave = Time.realtimeSinceStartup;
            sceneChanged = false;
        }
    }
    
    

    static void MyUpdateMethod()
    {
    }

    private void LogPlayModeState(PlayModeStateChange state)
    {
        if(Time.realtimeSinceStartup >= lastSave)
        {
            EditorApplication.SaveScene();
#if UNITY_EDITOR
            EditorApplication.playModeStateChanged -= LogPlayModeState;
#endif
            lastSave = Time.realtimeSinceStartup;
        }
    }

    private void SceneChanged(Scene scene)
    {
        sceneChanged = true;
        EditorApplication.SaveScene();
#if UNITY_EDITOR
        EditorSceneManager.sceneDirtied -= SceneChanged;
#endif
    }
    
    
}
